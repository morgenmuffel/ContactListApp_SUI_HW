//
//  ContactListApp_SUI_HWApp.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

@main
struct ContactListApp_SUI_HWApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
