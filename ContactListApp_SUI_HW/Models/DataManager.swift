//
//  DataManager.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import Foundation

final class DataManager {
    
    static let shared = DataManager()
    
    let names = [
        "Егор",
        "Дмитрий",
        "Авраам",
        "Мойша",
        "Моисей",
        "Валерия",
        "Яна",
        "Святогор",
        "Анна",
        "Татьяна"
    ]
    let surnames = [
        "Смит",
        "Браун",
        "Пульченюк",
        "Леннон",
        "Аддамс",
        "Тюльпаненко",
        "Миллер",
        "Уотсон",
        "Андерсон",
        "Суэйзи"
    ]
    let emails = [
        "blamur@gmail.com",
        "wheel_store@yahoo.com",
        "whosgood@gmail.com",
        "mommy87@yahoo.com",
        "barbariska998@gmail.com",
        "mmmisscum@gmail.com",
        "yoga_master@yahoo.com",
        "mrpitty@yahoo.com",
        "armor_power111@gmail.com",
        "cloudy_princess@gmail.com"
    ]
    let phoneNumbers = [
        "+7 980 774 12 11",
        "+7 906 302 55 20",
        "+7 999 687 02 38",
        "+7 960 541 20 37",
        "+7 905 124 17 01",
        "+7 988 145 18 90",
        "+7 915 675 14 12",
        "+7 909 985 64 21",
        "+7 902 314 68 69",
        "+7 996 213 11 87"
    ]
    
    private init() {}
    
}
