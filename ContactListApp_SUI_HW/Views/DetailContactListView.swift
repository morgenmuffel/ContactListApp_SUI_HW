//
//  DetailContactListView.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

struct DetailContactListView: View {
    let persons: [Person]
    var body: some View {
        List(persons) { person in
            Section {
                ContactRowView(imageName: "phone", text: person.phoneNumber)
                ContactRowView(imageName: "tray", text: person.email)
            } header: {
                Text("\(person.fullName)")
            }

        }
        .navigationTitle("Contact List")
    }
}

struct ContactListTwo_Previews: PreviewProvider {
    static var previews: some View {
        DetailContactListView(persons: Person.getPersons())
    }
}
