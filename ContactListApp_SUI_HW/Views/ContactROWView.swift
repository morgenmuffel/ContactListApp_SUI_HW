//
//  ContactROW.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

struct ContactRowView: View {
    let imageName: String
    let text: String
    var body: some View {
            HStack {
                Image(systemName:"\(imageName)")
                    .foregroundColor(.blue)
                Text("\(text)")
            }
    }
}

struct ContactRow_Previews: PreviewProvider {
    static var previews: some View {
        ContactRowView(imageName: "phone", text: "+7 916 916 91 69")
    }
}
