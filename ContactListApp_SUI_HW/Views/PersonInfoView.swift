//
//  PersonInfoView.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

struct PersonInfoView: View {
    let person: Person
    var body: some View {
        ZStack {
            Color.gray
                .ignoresSafeArea()
                .opacity(0.1)
            List {
                HStack {
                    Spacer()
                    Image(systemName: "person.fill")
                        .resizable()
                        .frame(width: 150, height: 150, alignment: .trailing)
                        .padding()
                    Spacer()
                }
                
                ContactRowView(imageName: "phone", text: person.phoneNumber)
                ContactRowView(imageName: "tray", text: person.email)
            }
            .navigationTitle("\(person.fullName)")
            .listStyle(.plain)
            .padding(.top, 40)
        }
        
    }
    
}

struct PersonsInfo_Previews: PreviewProvider {
    static var previews: some View {
        PersonInfoView(person: Person.getPerson())
    }
}
