//
//  ContentView.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

struct ContentView: View {
    let persons = Person.getPersons()
    
    var body: some View {
        
        TabView {
            NavigationView {
                GeneralContactListView(persons: persons)
            }
            .tabItem {
                Label("Contacts", systemImage: "person.3")
            }
            
            NavigationView {
                DetailContactListView(persons: persons)
            }
            .tabItem {
                Label("Numbers", systemImage: "phone")
            }
        }
        
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

