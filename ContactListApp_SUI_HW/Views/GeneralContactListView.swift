//
//  GeneralContactListView.swift
//  ContactListApp_SUI_HW
//
//  Created by Maksim on 26.12.2023.
//

import SwiftUI

struct GeneralContactListView: View {
    let persons: [Person]
    var body: some View {
        List(persons) { person in
            NavigationLink(destination: PersonInfoView(person: person)) {
                Text("\(person.fullName)")
            }
        }
        .listStyle(.plain)
        .navigationTitle("Contact List")
    }
    }


struct ContactListOne_Previews: PreviewProvider {
    static var previews: some View {
        GeneralContactListView(persons: Person.getPersons())
    }
}

